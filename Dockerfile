FROM composer AS builder

ENV COMPOSER_NO_INTERACTION 1
ENV COMPOSER_ALLOW_SUPERUSER 1

COPY composer.json composer.lock ./
RUN composer install

FROM php:fpm-alpine

COPY --from=builder /app/vendor ./vendor
COPY . .

EXPOSE 9000
